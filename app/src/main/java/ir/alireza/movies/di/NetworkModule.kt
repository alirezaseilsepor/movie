package ir.alireza.movies.di

import android.app.Application
import ir.alireza.movies.data.remote.interceptor.HeaderInterceptor
import ir.alireza.movies.data.response.NetworkResponseAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


const val BASE_URL_RELEASE = "https://api.themoviedb.org/3/"

const val RETROFIT = "RETROFIT"


val networkModule = module {


    single(named(RETROFIT)) { createRetrofit(get(), BASE_URL_RELEASE, androidApplication()) }

    single {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addInterceptor(HeaderInterceptor(get()))
            //   .addInterceptor(ConvertModelInterceptor(get()))
            .addInterceptor(httpLoggingInterceptor).build()
    }

}


fun createRetrofit(okHttpClient: OkHttpClient, url: String, app: Application): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addCallAdapterFactory(NetworkResponseAdapterFactory(app))
        .addConverterFactory(MoshiConverterFactory.create()).build()
}




