package ir.alireza.movies.di

import ir.alireza.movies.data.remote.api.*
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.create

val restModule = module {
    single<MovieApi> {  (get<Retrofit>(named(RETROFIT))).create() }
}