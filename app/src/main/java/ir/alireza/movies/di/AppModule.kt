package ir.alireza.movies.di

import android.os.Handler
import android.os.Looper
import com.squareup.moshi.Moshi
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val appModule = module {
    single { Moshi.Builder().build() }
    factory { Handler(Looper.getMainLooper()) }
}