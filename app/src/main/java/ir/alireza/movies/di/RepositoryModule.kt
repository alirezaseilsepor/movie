package ir.alireza.movies.di

import ir.alireza.movies.data.remote.repository.MovieRepository
import ir.alireza.movies.data.remote.repository.MovieRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {

    factory<MovieRepository> { MovieRepositoryImpl(get()) }

}