package ir.alireza.movies.view.fragment

import androidx.navigation.fragment.navArgs
import ir.alireza.movies.R
import ir.alireza.movies.base.BaseFragment
import ir.alireza.movies.ext.load
import kotlinx.android.synthetic.main.fragment_detail_movie.*

class DetailMovieFragment: BaseFragment(R.layout.fragment_detail_movie) {

    private val args: DetailMovieFragmentArgs by navArgs()

    override fun afterCreateView() {
        super.afterCreateView()
        with(args){
            image_slider.load("https://image.tmdb.org/t/p/w500${movie.backdropPath}")
            tv_title.text=movie.title
            tv_description.text=movie.overview
        }

    }
}