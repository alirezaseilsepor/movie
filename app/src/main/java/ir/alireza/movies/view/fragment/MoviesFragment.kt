package ir.alireza.movies.view.fragment

import ir.alireza.movies.R
import ir.alireza.movies.base.BaseFragment
import ir.alireza.movies.ext.visibleOrGone
import ir.alireza.movies.view.adapter.MovieAdapter
import ir.alireza.movies.vm.MoviesViewModel
import kotlinx.android.synthetic.main.fragment_movies.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MoviesFragment : BaseFragment(R.layout.fragment_movies) {

    private val viewModel: MoviesViewModel by viewModel()
    private val adapter = MovieAdapter()

    override fun firstCreateView() {
        super.firstCreateView()
        viewModel.getPopularMovies()
        adapter.onItemClickListener = { movie ->
            val action = MoviesFragmentDirections.actionMoviesFragmentToDetailMovieFragment(movie)
            navigate(action)
        }
        recycler_view.adapter = adapter
    }


    override fun initObserveViewModel() {
        super.initObserveViewModel()
        viewModel.listMovie.observe(viewLifecycleOwner, { listMovie ->
            adapter.submitList(listMovie)
        })
        viewModel.showProgressbar.observe(viewLifecycleOwner, { isVisible ->
            progress_bar.visibleOrGone(isVisible)
        })
    }
}