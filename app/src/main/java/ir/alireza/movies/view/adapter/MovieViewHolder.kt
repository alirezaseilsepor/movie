package ir.alireza.movies.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ir.alireza.movies.data.model.Movie
import ir.alireza.movies.ext.load
import ir.alireza.movies.ext.setOnSingleClickListener
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieViewHolder(itemView: View,private val onItemClickListener: ((Movie) -> Unit)?) : RecyclerView.ViewHolder(itemView) {
    fun bind(movie: Movie) = with(itemView) {
        image_movie.load("https://image.tmdb.org/t/p/w500${movie.posterPath}")
        tv_title.text=movie.title
        setOnSingleClickListener {
            onItemClickListener?.invoke(movie)
        }
    }
}