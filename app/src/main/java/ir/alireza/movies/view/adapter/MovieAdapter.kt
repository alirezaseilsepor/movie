package ir.alireza.movies.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.alireza.movies.R
import ir.alireza.movies.data.model.Movie


class MovieAdapter : ListAdapter<Movie, MovieViewHolder>(DIFF_CALLBACK) {

    var onItemClickListener: ((Movie) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false),
            onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<Movie> = object : DiffUtil.ItemCallback<Movie>() {
           override fun areItemsTheSame(oldMovie: Movie, newMovie: Movie): Boolean {
                return oldMovie.id == newMovie.id
            }

           override fun areContentsTheSame(oldUser: Movie,newUser: Movie): Boolean {
                return oldUser == newUser
           }
        }
    }
}