/*
 * *
 *  * Created by Alireza Seilsepor on 4/3/20 9:11 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/3/20 9:11 AM
 *
 */

package ir.alireza.movies.base

import androidx.annotation.IdRes
import androidx.navigation.NavDirections

interface FragmentNavigate {

    fun navigate(navDirections: NavDirections)
    fun navigate(@IdRes id: Int)

}