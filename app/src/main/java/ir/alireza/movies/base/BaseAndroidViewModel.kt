package ir.alireza.movies.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseAndroidViewModel(app: Application) : AndroidViewModel(app) {
    protected val _showProgressbar = MutableLiveData<Boolean>()
    val showProgressbar: LiveData<Boolean> get() = _showProgressbar

}