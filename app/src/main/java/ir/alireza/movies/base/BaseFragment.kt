package ir.alireza.movies.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import ir.alireza.movies.ext.hideKeyboard
import org.koin.android.ext.android.inject


abstract class BaseFragment(@LayoutRes layout: Int, private val firstTimeCreated: Boolean = true) :
    Fragment(layout), ViewCreatedFragment,
    FragmentOnBackPress, FragmentNavigate, FragmentResult {

    private var viewFragment: View? = null
    private var isFirstCreated = true
    private val handler: Handler by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (viewFragment == null || !firstTimeCreated) {
            isFirstCreated = true
            viewFragment = super.onCreateView(inflater, container, savedInstanceState)
        } else
            isFirstCreated = false
        beforeCreateView()
        return viewFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        afterCreateView()
        initObserveViewModel()
        if (isFirstCreated)
            firstCreateView()
    }


    override fun onDetach() {
        viewFragment = null
        super.onDetach()
    }


    override fun onStop() {
         handler.removeCallbacksAndMessages(null)
        super.onStop()
    }


    fun delay(time: Long, action: () -> Unit) {
        handler.postDelayed({ action.invoke() }, time)
    }

    override fun initObserveViewModel() {
    }

    override fun afterCreateView() {
    }

    override fun beforeCreateView() {
    }

    override fun firstCreateView() {
    }

    override fun onBackPressed() {
        requireView().hideKeyboard()
        requireActivity().onBackPressed()
    }

    override fun navigate(navDirections: NavDirections) {
        requireView().hideKeyboard()
        findNavController().navigate(navDirections)
    }

    override fun navigate(id: Int) {
        requireView().hideKeyboard()
        findNavController().navigate(id)
    }

    override fun onActivityResult(result: String?) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1000) {
            val result = data!!.getStringExtra("result")
            onActivityResult(result)
        }
    }
}