/*
 * *
 *  * Created by Alireza Seilsepor on 3/27/20 5:38 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/27/20 5:38 AM
 *
 */

package ir.alireza.movies.base

interface ViewCreatedFragment {

    fun initObserveViewModel()

    fun afterCreateView()

    fun beforeCreateView()

    fun firstCreateView()
}