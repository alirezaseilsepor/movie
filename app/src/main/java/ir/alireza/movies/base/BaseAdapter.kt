package ir.alireza.movies.base

import android.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T>(diffCallback: DiffUtil.ItemCallback<T>) :
    androidx.recyclerview.widget.ListAdapter<T, RecyclerView.ViewHolder>(diffCallback)