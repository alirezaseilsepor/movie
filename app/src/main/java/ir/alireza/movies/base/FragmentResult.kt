package ir.alireza.movies.base

interface FragmentResult {
    fun onActivityResult(result:String?)
}