/*
 * *
 *  * Created by Alireza Seilsepor on 4/3/20 3:58 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/3/20 3:58 AM
 *
 */

package ir.alireza.movies.base

interface FragmentOnBackPress {
    fun onBackPressed()
}