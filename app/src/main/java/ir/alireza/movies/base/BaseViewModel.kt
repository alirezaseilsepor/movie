package ir.alireza.movies.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
    protected val _showProgressbar = MutableLiveData<Boolean>()
    val showProgressbar: LiveData<Boolean> get() = _showProgressbar

}