package ir.alireza.movies.base

import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import ir.alireza.movies.app.MovieApp

import org.koin.android.ext.android.inject

abstract class BaseActivity(@LayoutRes layout: Int) : AppCompatActivity(layout),
    ViewCreatedActivity {

    private val handler: Handler by inject()



    override fun onCreate(savedInstanceState: Bundle?) {
        beforeCreateView()
        super.onCreate(savedInstanceState)
        afterCreateView()
    }

    override fun afterCreateView() {
    }

    override fun beforeCreateView() {
    }


    fun delay(time: Long, action: () -> Unit) {
        handler.postDelayed({ action.invoke() }, time)
    }
}