/*
 * *
 *  * Created by Alireza Seilsepor on 3/27/20 6:59 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/27/20 6:59 AM
 *
 */

package ir.alireza.movies.base

interface ViewCreatedActivity {

    fun afterCreateView()

    fun beforeCreateView()

}