package ir.alireza.movies.ext

/**
 * converts seconds to HH:MM:SS
 */
fun Long.toHumanReadableHMSTime(): String {
    val hours = this / 3600
    val minutes = (this % 3600) / 60
    val seconds = this % 60
    return if (hours != 0L) {
        String.format("%02d:%02d:%02d", hours, minutes, seconds)
    } else {
        String.format("%02d:%02d", minutes, seconds)
    }
}