/*
 * *
 *  * Created by Alireza Seilsepor on 4/6/20 8:39 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/6/20 8:39 AM
 *
 */

@file:Suppress("unused")

package ir.alireza.movies.ext

import android.app.Activity
import android.content.Intent
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import ir.alireza.movies.base.BaseActivity
import com.google.android.material.snackbar.Snackbar


inline fun <T> Fragment.onFragmentResultLiveDate(
    key: String = "result",
    crossinline block: (T) -> Unit
) {
    findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)
        ?.observe(viewLifecycleOwner,
            Observer { if (it.isNotNull()) block.invoke(it) })
}


fun <T> Fragment.setOnFragmentResult(result: T, key: String = "result") {
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}

inline fun <reified T : BaseActivity> Fragment.openActivityForResult(requestCode: Int = 1000, block: Intent.() -> Unit = {}) {
    val intent = Intent(requireContext(), T::class.java)
    block(intent)
    startActivityForResult(intent, requestCode)
}

fun Fragment.setActivityResult(result: String, key: String = "result") {
    val returnIntent = Intent()
    returnIntent.putExtra(key, result)
    requireActivity().setResult(Activity.RESULT_OK, returnIntent)
}


fun Fragment.getHeightScreen(): Int {
    val displayMetrics = DisplayMetrics()
    requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}



/*inline fun Fragment.showSnackBar(
    @StringRes messageRes: Int,
    length: Int = Snackbar.LENGTH_LONG,
    @ColorRes color: Int,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar(resources.getString(messageRes), length, color, f)
}

inline fun Fragment.showSnackBar(
    message: String,
    length: Int = Snackbar.LENGTH_LONG,
    @ColorRes color: Int,
    f: Snackbar.() -> Unit
): Snackbar {
    val snack = Snackbar.make(requireView(), message, length)
    snack.view.layoutDirection = View.LAYOUT_DIRECTION_RTL
    snack.setBackGroundColor(color)
    snack.f()
    snack.show()
    return snack
}*/


inline fun Fragment.showSnackBar(
    message: String = "",
    f: Snackbar.() -> Unit = {}
): Snackbar {
    val snack = Snackbar.make(requireView(), "", Snackbar.LENGTH_LONG)
    snack.view.layoutDirection = View.LAYOUT_DIRECTION_RTL
    (snack.view.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.TOP
    snack.setText(message)
    snack.f()
    snack.show()
    return snack
}

inline fun Fragment.showSuccessSnackBar(
    @StringRes message: Int,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar {
        setText(message)
        setBackGroundColor(android.R.color.holo_green_dark)
        f()
    }
}

inline fun Fragment.showErrorSnackBar(
    @StringRes message: Int,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar {
        setText(message)
        setBackGroundColor(android.R.color.holo_red_dark)
        f()
    }
}

inline fun Fragment.showErrorSnackBar(
    message: String,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar {
        setText(message)
        setBackGroundColor(android.R.color.holo_red_dark)
        f()
    }
}

inline fun Fragment.showErrorSnackBar(
    @StringRes message: Int,
    extraString: Int? = null,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar {
        if (extraString == null)
            setText(message)
        else
            setText(getString(message, extraString))
        setBackGroundColor(android.R.color.holo_red_dark)
        f()
    }
}

