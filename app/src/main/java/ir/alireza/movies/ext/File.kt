/*
 * *
 *  * Created by Alireza Seilsepor on 4/5/20 4:20 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/5/20 4:20 AM
 *
 */

package ir.alireza.movies.ext

import android.webkit.MimeTypeMap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


/**
 * converts a file to multipart request
 */
fun File.toMultipart(name: String): MultipartBody.Part {
    var type: String? = null
    val extension = MimeTypeMap.getFileExtensionFromUrl(path)
    if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
    val requestFile =
        asRequestBody("$type".toMediaTypeOrNull()!!)
    return MultipartBody.Part.createFormData(name, this.name, requestFile)
}