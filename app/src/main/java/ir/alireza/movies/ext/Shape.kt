package ir.alireza.movies.ext

import android.content.Context
import androidx.annotation.ColorRes
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel

fun Context.roundRectShape(
    radius: Int = 12,
    @ColorRes fillColor: Int = android.R.color.transparent,
    @ColorRes strokeColor: Int = android.R.color.transparent,
    strokeWidth: Float = 0f
): MaterialShapeDrawable {
    val shapeDrawable = MaterialShapeDrawable()
    shapeDrawable.fillColor = getColorCompatStateList(fillColor)
    shapeDrawable.strokeWidth = strokeWidth
    shapeDrawable.strokeColor = getColorCompatStateList(strokeColor)
    var shapeAppearanceModel = ShapeAppearanceModel()
    shapeAppearanceModel = shapeAppearanceModel.toBuilder().setAllCorners(
        CornerFamily.ROUNDED,
        radius.px
    ).build()
    shapeDrawable.shapeAppearanceModel = shapeAppearanceModel
    return shapeDrawable
}



fun Context.circleShape(
    @ColorRes fillColor: Int = android.R.color.transparent,
    @ColorRes strokeColor: Int = android.R.color.transparent,
    strokeWidth: Float = 0f
): MaterialShapeDrawable {
    val shapeDrawable = MaterialShapeDrawable()
    shapeDrawable.fillColor = getColorCompatStateList(fillColor)
    shapeDrawable.strokeWidth = strokeWidth
    shapeDrawable.strokeColor = getColorCompatStateList(strokeColor)
    var shapeAppearanceModel = ShapeAppearanceModel()
    shapeAppearanceModel = shapeAppearanceModel.toBuilder().setAllCorners(
        CornerFamily.ROUNDED,
        200.px
    ).build()
    shapeDrawable.shapeAppearanceModel = shapeAppearanceModel
    return shapeDrawable
}