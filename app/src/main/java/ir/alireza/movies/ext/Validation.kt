/*
 * *
 *  * Created by Alireza Seilsepor on 3/30/20 5:36 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/30/20 5:36 AM
 *
 */

package ir.alireza.movies.ext

import android.util.Patterns
import java.util.regex.Pattern

/**
 * Just checking if the input string is a home telephone number (Iranian type)
 * Matching 0XXXXXXXXX or 0*
 *
 * @param whole decides if this function is validating a whole 0XXXXXXXXX number
 */
//fixme: this function validates 021XXXXXXXXXXXXX which is not a valid (0)(2d)(8d) in iran.
fun String.isTelephone(whole: Boolean=true): Boolean {
    val regex = arrayOf(
        "^(0[1-8]{1}|[1-8]{1})${if (whole) "[0-9]{9}" else ".*"}\$"
    )
    return Pattern.compile(regex[0], Pattern.MULTILINE)
        .matcher(this).matches()
}

fun String.isMobileNumber(whole: Boolean = true): Boolean {
    return length==11 && substring(0,2)=="09"
    /*return matchesHamrahAvvalPhone(whole) ||
            matchesIrancellPhone(whole) ||
            matchesRightelPhone(whole) ||
            matchesTaliaPhone(whole)*/
}

//fixme: this function validates 0912XXXXXXXXXXXXX which is not a valid (0)(3d)(7d) in iran.
fun String.matchesIrancellPhone(whole: Boolean): Boolean {
    val regex = arrayOf(
        "^(090|90|\\+9890|9890)[1235]${if (whole) "[0-9]{7}" else ".*"}\$",
        "^(093|93|\\+9893|9893)[0356789]${if (whole) "[0-9]{7}" else ".*"}\$"
    )

    var res = false
    regex.forEach {
        res = res || Pattern.compile(it, Pattern.MULTILINE)
            .matcher(this).matches()
    }
    return res
}


//fixme: this function validates 0912XXXXXXXXXXXXX which is not a valid (0)(3d)(7d) in iran.
fun String.matchesRightelPhone(whole: Boolean): Boolean {
    val regex = arrayOf(
        "^(092|92|\\+9892|9892)[012]${if (whole) "[0-9]{7}" else ".*"}\$"
    )

    return Pattern.compile(regex[0], Pattern.MULTILINE)
        .matcher(this).matches()
}

//fixme: this function validates 0912XXXXXXXXXXXXX which is not a valid (0)(3d)(7d) in iran.
fun String.matchesHamrahAvvalPhone(whole: Boolean): Boolean {
    val regex = arrayOf(
        "^(091|91|\\+9891|9891)${if (whole) "[0-9]{8}" else ".*"}\$",
        "^(099|99|\\+9899|9899)[0-2]${if (whole) "[0-9]{7}" else ".*"}\$"
    )

    var res = false
    regex.forEach {
        res = res || Pattern.compile(it, Pattern.MULTILINE)
            .matcher(this).matches()
    }
    return res
}

//fixme: this function validates 0912XXXXXXXXXXXXX which is not a valid (0)(3d)(7d) in iran.
fun String.matchesTaliaPhone(whole: Boolean): Boolean {
    val regex = arrayOf(
        "^(0932|932|\\+98932|98932)${if (whole) "[0-9]{7}" else ".*"}\$"
    )

    var res = false
    regex.forEach {
        res = res || Pattern.compile(it, Pattern.MULTILINE)
            .matcher(this).matches()
    }
    return res
}


fun String.isAllEnglishChars() =
    Pattern.compile("^[a-zA-Z0-9@&-_. ]+\$")
        .matcher(this)
        .matches()


fun String?.isEmail(): Boolean {
    return this?.let {
        return Patterns.EMAIL_ADDRESS.matcher(it).matches()
    } ?: false
}


fun Any?.isNull(): Boolean {
    return this == null
}

fun Any?.isNotNull(): Boolean {
    return this != null
}


//https://stackoverflow.com/questions/12018245/regular-expression-to-validate-username
fun String.isUserName(): Boolean {
    /*return  Pattern.compile("^([a-zA-Z])+([\\w]{2,})+$")
        .matcher(this)
        .matches() */

    return  Pattern.compile("^(?=.{5,25}\$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])\$")
        .matcher(this)
        .matches()
}


/**
 * Checks the input string with Iran's national code and returns true if it's valid.
 * ref to https://www.dotnettips.info/post/1097
 */
fun String?.isNationalCode(): Boolean {
    return this?.let { code ->
        if (code.length != 10) return false
        var sum = 0
        for (i in 0 until code.length - 1)
            sum += (10 - i) * code[i].toString().toInt()

        val res = sum % 11
        return if (res < 2)
            res == code[9].toString().toInt()
        else
            res == 11 - code[9].toString().toInt()

    } ?: false

}