/*
 * *
 *  * Created by Alireza Seilsepor on 4/5/20 4:58 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/5/20 4:58 AM
 *
 */

package ir.alireza.movies.ext

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import androidx.core.net.toFile
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*


fun createImageFile(): File {
    val storageDir = File(Environment.getDownloadCacheDirectory(), "Pictures")
    if (!storageDir.exists())
        storageDir.mkdirs()
    return File.createTempFile(
        "JPEG_${UUID.randomUUID()}_",
        ".jpg",
        storageDir
    ).apply {
        deleteOnExit()
    }
}


fun Uri.toImageFile(context: Context?): File? {
    try {
        return toFile()
    } catch (e: Exception) {
        try {
            val inputStream = context?.contentResolver?.openInputStream(this)
            val file = createImageFile()
            val outputStream = FileOutputStream(file)
            val buf = ByteArray(1024)
            var len = 0
            if (inputStream == null) return null
            while ({ len = inputStream.read(buf); len }() != -1) {
                outputStream.write(buf, 0, len)
            }
            outputStream.flush()
            outputStream.close()
            inputStream.close()
            return file
        } catch (e: Exception) {
            return null
        }
    }
}




fun Drawable.toBitmap(): Bitmap? {
    if (this is BitmapDrawable) {
        val bitmapDrawable = this
        if (bitmapDrawable.bitmap != null) {
            return bitmapDrawable.bitmap
        }
    }
    val bitmap = if (this.intrinsicWidth <= 0 || this.intrinsicHeight <= 0) {
        Bitmap.createBitmap(
            1,
            1,
            Bitmap.Config.ARGB_8888
        ) // Single color bitmap will be created of 1x1 pixel
    } else {
        Bitmap.createBitmap(
            this.intrinsicWidth,
            this.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
    }
    val canvas = Canvas(bitmap)
    this.setBounds(0, 0, canvas.width, canvas.height)
    this.draw(canvas)
    return bitmap
}

fun Bitmap.toFile(application: Application, name: String = System.nanoTime().toString()): File {
    val filesDir: File = application.filesDir
    val imageFile = File(filesDir, "$name.jpg")
    val os: OutputStream
    try {
        os = FileOutputStream(imageFile)
        compress(Bitmap.CompressFormat.JPEG, 100, os)
        os.flush()
        os.close()
    } catch (e: java.lang.Exception) {

    }
    return imageFile
}