/*
 * *
 *  * Created by Alireza Seilsepor on 4/6/20 9:23 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/6/20 9:23 AM
 *
 */

@file:Suppress("unused")

package ir.alireza.movies.ext

import android.app.Activity
import android.content.Intent
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import ir.alireza.movies.R
import ir.alireza.movies.base.BaseActivity
import com.google.android.material.snackbar.Snackbar


inline fun <reified T : BaseActivity> Activity.openActivityForResult(
    requestCode: Int = 1000,
    block: Intent.() -> Unit = {}
) {
    val intent = Intent(this, T::class.java)
    block(intent)
    startActivityForResult(intent, requestCode)
}

fun Activity.setActivityResult(result: String, key: String = "result") {
    val returnIntent = Intent()
    returnIntent.putExtra(key, result)
    setResult(Activity.RESULT_OK, returnIntent)
}

fun Activity.getHeightScreen(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

fun Activity.shareText(shareText: String) {
    val sendIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, shareText)
        type = "text/plain"
    }
    val shareIntent = Intent.createChooser(sendIntent, null)
    startActivity(shareIntent)
}

inline fun <reified T : Any> Activity.extra(key: String, default: T? = null) = lazy {
    val value = intent?.extras?.get(key)
    if (value is T) value else default
}

inline fun <reified T : Any> Activity.extraNotNull(key: String, default: T? = null) = lazy {
    val value = intent?.extras?.get(key)
    requireNotNull(if (value is T) value else default) { key }
}

inline fun Activity.showSnackBar(
    message: String = "",
    f: Snackbar.() -> Unit = {}
): Snackbar {
    val viewGroup = (this.findViewById(R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
    val snack = Snackbar.make(viewGroup, "", Snackbar.LENGTH_LONG)
    snack.view.layoutDirection = View.LAYOUT_DIRECTION_RTL
    snack.setText(message)
    snack.f()
    snack.show()
    return snack
}


inline fun Activity.showSuccessSnackBar(
    @StringRes message: Int,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar {
        setText(message)
        setBackGroundColor(android.R.color.holo_green_dark)
        f()
    }
}

inline fun Activity.showErrorSnackBar(
    @StringRes message: Int,
    extraString: Int? = null,
    f: Snackbar.() -> Unit = {}
): Snackbar {
    return showSnackBar {
        if (extraString == null)
            setText(message)
        else
            setText(getString(message, extraString))
        setBackGroundColor(android.R.color.holo_red_dark)
        f()
    }
}


