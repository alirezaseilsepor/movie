package ir.alireza.movies.ext

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.constraintlayout.widget.Placeholder
import ir.alireza.movies.R
import com.bumptech.glide.Glide
import java.io.File

fun ImageView.load(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

fun ImageView.load(drawable: Drawable) {
    Glide.with(this)
        .load(drawable)
        .into(this)
}

fun ImageView.load(file: File) {
    Glide.with(this)
        .load(file)
        .into(this)
}

fun ImageView.load(uri: Uri) {
    Glide.with(this)
        .load(uri)
        .into(this)
}

fun ImageView.load(bitmap: Bitmap) {
    Glide.with(this)
        .load(bitmap)
        .into(this)
}

fun ImageView.loadCircle(url: String?) {
    Glide.with(this)
        .load(url)
        .circleCrop()
        .into(this)
}

fun ImageView.loadCircle(url: String?,placeholder: Int) {
    Glide.with(this)
        .load(url)
        .circleCrop()
        .placeholder(placeholder)
        .into(this)
}

fun ImageView.loadCircle(uri: Uri) {
    Glide.with(this)
        .load(uri)
        .circleCrop()
        .into(this)
}