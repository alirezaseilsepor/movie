package ir.alireza.movies.vm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import ir.alireza.movies.base.BaseViewModel
import ir.alireza.movies.data.model.Movie
import ir.alireza.movies.data.remote.repository.MovieRepository
import ir.alireza.movies.data.response.NetworkResponse
import kotlinx.coroutines.launch

class MoviesViewModel(private val movieRepository: MovieRepository):BaseViewModel() {

    private val _listMovie = MutableLiveData<List<Movie>>()
    val listMovie: LiveData<List<Movie>> get() = _listMovie


    fun getPopularMovies() {
        viewModelScope.launch {
            _showProgressbar.postValue(true)
            val response = movieRepository.getPopularMovies()
            _showProgressbar.postValue(false)
            when (response) {
                is NetworkResponse.Success -> {
                    _listMovie.value=response.body.data
                }
                is NetworkResponse.Empty -> {

                }
                else -> {

                }
            }
        }
    }
}