package ir.alireza.movies.app

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import ir.alireza.movies.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class MovieApp:Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MovieApp)
            modules(
                appModule,
                dbModule,
                networkModule,
                repositoryModule,
                restModule,
                viewModelModule,
                module {
                    single { this@MovieApp.contentResolver }
                }
            )
        }

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}