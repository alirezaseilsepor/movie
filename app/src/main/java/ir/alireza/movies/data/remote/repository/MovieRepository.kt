package ir.alireza.movies.data.remote.repository

import ir.alireza.movies.data.model.BaseResponse
import ir.alireza.movies.data.model.Movie
import ir.alireza.movies.data.model.MovieResult
import ir.alireza.movies.data.response.NetworkResponse


interface MovieRepository {

    suspend fun getPopularMovies(): NetworkResponse<MovieResult>

}