/**
 * Created by Saman Sattari on 5/14/2019.
 *
 * Copyright (c) 2019 MobtakerTeam All rights reserved.
 *
 */

package ir.alireza.movies.data.model

/**
 * and Object that holds error codes
 */
object ErrorCode {


    const val MODEL_HAVE_PROBLEM = 0
    const val NETWORK_ERROR = 9000
    const val NO_INTERNET = 9001
    const val NOT_EXTEND_BASERESPONSE = 9002
}