package ir.alireza.movies.data.model

import com.squareup.moshi.Json

const val NOT_EXTEND_BASE_RESPONSE = "NOT_EXTEND_BASE_RESPONSE"
const val FALSE = "FALSE"

data class BaseResponse(
    @field:Json(name = "page")
    val page: Int,
    @field:Json(name = "results")
    val data: List<Any>?,
    @field:Json(name = "total_pages")
    val totalPages: Int,
    @field:Json(name = "total_results")
    val totalResults: Int,
    @field:Json(name = "status_code")
    val statusCode: Int?
)