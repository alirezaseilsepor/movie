package ir.alireza.movies.data.model

import com.squareup.moshi.Json


data class MovieResult(
    @field:Json(name = "page")
    val page: Int,
    @field:Json(name = "results")
    val data: List<Movie>,
    @field:Json(name = "total_pages")
    val totalPages: Int,
    @field:Json(name = "total_results")
    val totalResults: Int,
    @field:Json(name = "status_code")
    val statusCode: Int?
)