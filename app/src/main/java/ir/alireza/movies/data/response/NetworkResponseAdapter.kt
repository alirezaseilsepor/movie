package ir.alireza.movies.data.response

import android.app.Application
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

class NetworkResponseAdapter<S : Any>(
    private val successType: Type/*,
    private val errorBodyConverter: Converter<ResponseBody, Any>*/,
     private val app: Application
) : CallAdapter<S, Call<NetworkResponse<S>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<S>): Call<NetworkResponse<S>> {
        return NetworkResponseCall(call/*, errorBodyConverter*/,app)
    }
}
