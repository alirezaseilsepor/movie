package ir.alireza.movies.data.response


sealed class NetworkResponse<out T : Any?> {
    /**
     * Success response with body
     */
    data class Success<T : Any>(val body: T) : NetworkResponse<T>()

    /**
     * Failure response with body
     */
    data class Error(val code: TypeApiError) : NetworkResponse<Nothing>()


    /**
     * Empty List
     */
    data class Empty(val error: Throwable? = null) : NetworkResponse<Nothing>()

    /*  */
    /**
     * Network error
     *//*
    data class NetworkError(val error: IOException) : NetworkResponse<Nothing, Nothing>()

    */
    /**
     * For example, json parsing error
     *//*
    data class UnknownError(val error: Throwable?) : NetworkResponse<Nothing, Nothing>()*/
}
