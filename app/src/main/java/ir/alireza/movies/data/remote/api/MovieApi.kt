package ir.alireza.movies.data.remote.api

import ir.alireza.movies.data.model.*
import ir.alireza.movies.data.response.NetworkResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MovieApi {

    @Headers("$NOT_EXTEND_BASE_RESPONSE:$FALSE")
    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("api_key") key: String): NetworkResponse<MovieResult>

}