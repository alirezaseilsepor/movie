package ir.alireza.movies.data.response

import android.app.Application
import ir.alireza.movies.data.model.ApiException
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

internal class NetworkResponseCall<S : Any>(
    private val delegate: Call<S>,
    private val app: Application
) : Call<NetworkResponse<S>> {

    override fun enqueue(callback: Callback<NetworkResponse<S>>) {
        return delegate.enqueue(object : Callback<S> {
            override fun onResponse(call: Call<S>, response: Response<S>) {
                val body = response.body()

                if (response.isSuccessful) {
                    if (body != null) {
                        callback.onResponse(
                            this@NetworkResponseCall,
                            Response.success(NetworkResponse.Success(body))
                        )
                    } else {
                        // Response is successful but the body is null
                        callback.onResponse(
                            this@NetworkResponseCall,
                            Response.success(
                                NetworkResponse.Error(TypeApiError.UNKNOWN_ERROR)
                            )
                        )
                    }
                } else {
                    callback.onResponse(
                        this@NetworkResponseCall,
                        Response.success(NetworkResponse.Error(TypeApiError.UNKNOWN_ERROR))
                    )
                }
            }

            override fun onFailure(call: Call<S>, throwable: Throwable) {
                if (throwable is ApiException)
                    when {
                        throwable.statusCode == 401 -> {
                            callback.onResponse(
                                this@NetworkResponseCall,
                                Response.success(NetworkResponse.Error(TypeApiError.EXPIRE_TOKEN))
                            )
                            return
                        }
                        else -> {
                            callback.onResponse(
                                this@NetworkResponseCall,
                                Response.success(NetworkResponse.Error(TypeApiError.UNKNOWN_ERROR))
                            )
                            return
                        }
                    }
                val networkResponse = when (throwable) {
                    is IOException -> NetworkResponse.Error(TypeApiError.NETWORK_ERROR)
                    else -> NetworkResponse.Error(TypeApiError.UNKNOWN_ERROR)
                }
                callback.onResponse(this@NetworkResponseCall, Response.success(networkResponse))
            }
        })
    }

    override fun isExecuted() = delegate.isExecuted

    override fun clone() = NetworkResponseCall(delegate.clone(), app)

    override fun isCanceled() = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<NetworkResponse<S>> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun request(): Request = delegate.request()

    override fun timeout(): Timeout = delegate.timeout()
}

enum class TypeApiError {
    NETWORK_ERROR, UNKNOWN_ERROR , EXPIRE_TOKEN
}
