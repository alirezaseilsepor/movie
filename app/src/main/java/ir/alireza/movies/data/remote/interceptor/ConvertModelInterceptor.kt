package ir.alireza.movies.data.remote.interceptor



import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import ir.alireza.movies.data.model.ApiException
import ir.alireza.movies.data.model.BaseResponse
import ir.alireza.movies.data.model.ErrorCode.MODEL_HAVE_PROBLEM
import ir.alireza.movies.data.model.ErrorCode.NETWORK_ERROR
import ir.alireza.movies.data.model.ErrorCode.NOT_EXTEND_BASERESPONSE
import ir.alireza.movies.data.model.FALSE
import ir.alireza.movies.data.model.NOT_EXTEND_BASE_RESPONSE
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody


class ConvertModelInterceptor(
    private val moshi: Moshi
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val request = chain.request()
            val isNotBaseResponse = request.header(NOT_EXTEND_BASE_RESPONSE) == FALSE
            val response = chain.proceed(chain.request())
            if (response.header("Content-Type") == "video/mpeg")
                return response
            var data = ""
            response.body?.string()?.let {
                data = moshi.extractData(it,isNotBaseResponse)
            }
            val contentType = response.body?.contentType()
            val body = ResponseBody.create(contentType, data)
            return response.newBuilder().body(body).build()
        } catch (e: ApiException) {
            if (e.statusCode == NOT_EXTEND_BASERESPONSE)
                return chain.proceed(chain.request())
            throw e
        } catch (e: Exception) {
            throw ApiException(NETWORK_ERROR, e)
        }
    }

}

/**
 * this func checks status code and handles response
 * if response is 200 then data returns otherwise it will throw an exception
private fun Gson.extractData(responseString: String): String {
val apiResponse: BaseResponse = fromJson(responseString, BaseResponse::class.java)

if (apiResponse.code == 804)
throw ApiException(apiResponse.code)

if (apiResponse.data == null && apiResponse.code == null)
throw ApiException(NOT_EXTEND_BASERESPONSE)


if (apiResponse.code != null && apiResponse.code < 1000) {
return if (apiResponse.data == null) "{}" else toJson(apiResponse.data)
} else throw ApiException(apiResponse.code ?: MODEL_HAVE_PROBLEM)
}*/

/**
 * this func checks status code and handles response
 * if response is 200 then data returns otherwise it will throw an exception
 */
private fun Moshi.extractData(responseString: String,isNotBaseResponse:Boolean): String {
    val jsonAdapter: JsonAdapter<BaseResponse> = adapter(BaseResponse::class.java)
    val apiResponse: BaseResponse = jsonAdapter.fromJson(responseString)!!

    if (apiResponse.data == null)
        throw ApiException(NOT_EXTEND_BASERESPONSE,messageError = "body null")

    if (isNotBaseResponse)
        throw ApiException(NOT_EXTEND_BASERESPONSE,messageError = "not extend BadeResponse")



    if (apiResponse.statusCode != null) {
        return adapter(Any::class.java).toJson(apiResponse.data)
    } else throw ApiException(apiResponse.statusCode ?: MODEL_HAVE_PROBLEM)
}