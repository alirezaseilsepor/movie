package ir.alireza.movies.data.remote.repository

import ir.alireza.movies.data.model.BaseResponse
import ir.alireza.movies.data.model.Movie
import ir.alireza.movies.data.model.MovieResult
import ir.alireza.movies.data.remote.api.MovieApi
import ir.alireza.movies.data.response.NetworkResponse


class MovieRepositoryImpl(private val movieApi: MovieApi) : MovieRepository {

    companion object{
        private const val APP_KEY="837d7c86b635c51b72295f6c5cacb94a"
    }

    override suspend fun getPopularMovies(): NetworkResponse<MovieResult> {
        return movieApi.getPopularMovies(APP_KEY)
    }
}