/*
 * *
 *  * Created by Alireza Seilsepor on 4/3/20 2:41 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/3/20 2:41 AM
 *
 */

package ir.alireza.movies.data.remote.interceptor

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.os.Build
import android.provider.Settings
import ir.alireza.movies.ext.getAndroidVersion
import okhttp3.Interceptor
import okhttp3.Response

private const val DEVICE_PLATFORM = "Device_Platform"
private const val DEVICE_PLATFORM_VALUE = "Android_App"
private const val USER_AGENT = "User-Agent"
private const val USER_AGENT_VALUE = "Movie"
private const val DEVICE_ID = "DEVICE_ID"

@SuppressLint("HardwareIds")
class HeaderInterceptor(private val contentResolver: ContentResolver) : Interceptor {
    private val androidOs = getAndroidVersion(Build.VERSION.SDK_INT)
    private val androidId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
             //   .addHeader(DEVICE_PLATFORM, DEVICE_PLATFORM_VALUE)
             //   .header(USER_AGENT, "$USER_AGENT_VALUE $androidOs")
            //    .header(DEVICE_ID, androidId)
                .build()
        )
    }
}