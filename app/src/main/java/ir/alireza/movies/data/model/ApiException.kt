package ir.alireza.movies.data.model

import java.io.IOException

/**
 * Custom Exception class for handling server errors
 */
class ApiException(val statusCode: Int, e: Throwable? = null,var messageError:String="") : IOException(e)